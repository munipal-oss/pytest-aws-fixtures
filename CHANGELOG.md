## [3.0.0](https://gitlab.com/munipal-oss/pytest-aws-fixtures/compare/v2.0.0...v3.0.0) (2024-10-07)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pre-commit ^3.6.0

### Miscellaneous Chores

* **deps:** update dependency pre-commit to v4 ([137f1fb](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/137f1fb002aef9f07833d20645d8b95066748458))

## [2.0.0](https://gitlab.com/munipal-oss/pytest-aws-fixtures/compare/v1.3.0...v2.0.0) (2024-07-30)

### ⚠ BREAKING CHANGES

* **deps:** drop support for tenacity ^8.2.3

### Bug Fixes

* **deps:** update dependency tenacity to v9 ([586a3f9](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/586a3f92b29d1843d65887d792b93142e6d4e2f8))

## [1.3.0](https://gitlab.com/munipal-oss/pytest-aws-fixtures/compare/v1.2.0...v1.3.0) (2024-07-25)

### Features

* add additional fixtures ([a9109b0](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/a9109b0b1946247be17818d708c94e37a72b8028))

## [1.2.0](https://gitlab.com/munipal-oss/pytest-aws-fixtures/compare/v1.1.1...v1.2.0) (2024-02-02)


### Features

* add fixture to poll for a specific step function execution ARN ([7280259](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/7280259e47419fdcbdfbb6ad1fa45feb22317fb7))

## [1.1.1](https://gitlab.com/munipal-oss/pytest-aws-fixtures/compare/v1.1.0...v1.1.1) (2024-02-02)


### Bug Fixes

* use correct fixture name for polling for lambda update status ([e9d88a5](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/e9d88a520ea65899e046e1a80885ff5d2002430d))

## [1.1.0](https://gitlab.com/munipal-oss/pytest-aws-fixtures/compare/v1.0.0...v1.1.0) (2024-02-02)


### Features

* add sts_client fixture ([0b304e7](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/0b304e7a37d7af4454d1567b4b471f3e69cc660e))

## 1.0.0 (2024-02-02)


### Features

* initial release ([d7ae0f7](https://gitlab.com/munipal-oss/pytest-aws-fixtures/commit/d7ae0f76ca16e00ed5331a9e3b5c1b8ff76d0fbc))
